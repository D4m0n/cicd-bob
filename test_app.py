import unittest

from app import app

app.testing = True


class UnitTest(unittest.TestCase):
    def test_add(self):
        params = {'a': 3, 'b': 5}
        with app.test_client() as client:
            response = client.get('/add', query_string = params)
            result = int(response.data)
            self.assertEqual(8, result)


    def test_sub(self):
        params = {'a': 3, 'b': 5}
        with app.test_client() as client:
            response = client.get('/sub', query_string = params)
            result = int(response.data)
            self.assertEqual(-2, result)


if __name__ == '__main__':
    unittest.main()
