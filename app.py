import json

from flask import Flask, request


app = Flask(__name__)


@app.route('/')
def index():
    return f'hi'


@app.route('/add', methods=['GET'])
def add():
    a = int(request.args['a'])
    b = int(request.args['b'])

    return f'{a + b}'


@app.route('/sub', methods=['GET'])
def sub():
    a = int(request.args['a'])
    b = int(request.args['b'])

    return f'{a - b}'



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8185)
